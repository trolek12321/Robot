using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonetyManager : MonoBehaviour
{
    public static int MonetyCount;
    public int MonetyWidoczne;
    public TMPro.TextMeshProUGUI textTmp2;
    void Start()
    {
        MonetyManager.MonetyCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        MonetyWidoczne = MonetyCount;
        textTmp2.text = MonetyCount.ToString();
    }
}
