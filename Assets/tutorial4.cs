using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorial4 : MonoBehaviour
{
    public GameObject lightning;
    public GameObject player;
    public GameObject kamera;
    public GameObject tutorial3;
    public GameObject tutorial5;

    public GameObject textMeshPro;
    public GameObject energie;

    bool hpdown = false;

    int da = 5;

    void Start()
    {
        textMeshPro.SetActive(false);
        StartCoroutine("NextTutorial2");
    }

    IEnumerator NextTutorial2()
    {
        while (true)
        {
            if (!lightning.activeSelf)
            {
                energie.SetActive(false);
                textMeshPro.SetActive(true);
                while (da > 0)
                {
                    da--;
                    Debug.Log(da);
                    if (da == 0)
                    {
                        tutorial3.SetActive(false);
                        kamera.transform.position = new Vector3(151, 2, -20);
                        player.transform.position = new Vector3(158, -1, -19);
                        tutorial5.SetActive(true);
                        yield break; // Zako�cz coroutine po wykonaniu
                    }
                    yield return new WaitForSeconds(1f);
                }
            }
            yield return null;
        }
    }
}
