using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class tutorial : MonoBehaviour
{
    private HashSet<KeyCode> pressedKeys = new HashSet<KeyCode>();
    private List<KeyCode> expectedKeys = new List<KeyCode>();

    public TMPro.TextMeshProUGUI KeyW;
    public TMPro.TextMeshProUGUI KeyS;
    public TMPro.TextMeshProUGUI KeyA;
    public TMPro.TextMeshProUGUI KeyD;

    public GameObject keys;
    public GameObject tutorial1;
    public GameObject kamera;
    public GameObject player;
    public GameObject tutorial2;

    KeyCode UP;
    KeyCode DOWN;
    KeyCode LEFT;
    KeyCode RIGHT;

    bool UP_t = false;
    bool DOWN_t = false;
    bool LEFT_t = false;
    bool RIGHT_t = false;

    private void Start()
    {
        Tutorial1();
    }

    void Tutorial1()
    {
        if (SettingsMenu.instance == null)
        {
            UP = KeyCode.W;
            DOWN = KeyCode.S;
            LEFT = KeyCode.A;
            RIGHT = KeyCode.D;

            KeyW.text = "W";
            KeyS.text = "S";
            KeyA.text = "A";
            KeyD.text = "D";
        }
        else
        {
            string upKey = SettingsMenu.instance.upDropdown.options[SettingsMenu.instance.upDropdown.value].text;
            string downKey = SettingsMenu.instance.downDropdown.options[SettingsMenu.instance.downDropdown.value].text;
            string leftKey = SettingsMenu.instance.leftDropdown.options[SettingsMenu.instance.leftDropdown.value].text;
            string rightKey = SettingsMenu.instance.rightDropdown.options[SettingsMenu.instance.rightDropdown.value].text;

            UP = (KeyCode)Enum.Parse(typeof(KeyCode), upKey);
            DOWN = (KeyCode)Enum.Parse(typeof(KeyCode), downKey);
            LEFT = (KeyCode)Enum.Parse(typeof(KeyCode), leftKey);
            RIGHT = (KeyCode)Enum.Parse(typeof(KeyCode), rightKey);

            KeyW.text = upKey;
            KeyS.text = downKey;
            KeyA.text = leftKey;
            KeyD.text = rightKey;
        }

    }

    private void Update()
    {
        if(Input.GetKeyDown(UP))
        {
            UP_t = true;
        }

        if(Input.GetKeyDown(DOWN)) 
        { 
            DOWN_t = true; 
        }

        if (Input.GetKeyDown(LEFT)) 
        {
            LEFT_t = true;
        }

        if (Input.GetKeyDown(RIGHT))
        {
            RIGHT_t = true;
        }

        if(UP_t && DOWN_t && LEFT_t && RIGHT_t)
        {
            keys.SetActive(false);
            tutorial1.SetActive(false);
            tutorial2.SetActive(true);
            kamera.transform.position = new Vector3(36, 2, -20);
            player.transform.position = new Vector3(43, -1, -19);
            

        }
    }

    private string GetKeyText(string keyName, string defaultValue)
    {
        if (!string.IsNullOrEmpty(keyName))
        {
            return keyName;
        }
        else
        {
            return defaultValue;
        }
    }

    private KeyCode GetKeyCodeOrDefault(string keyName, KeyCode defaultValue)
    {
        if (Enum.TryParse(keyName, out KeyCode keyCode))
        {
            return keyCode;
        }
        else
        {
            Debug.LogWarning("Invalid key name: " + keyName);
            return defaultValue;
        }
    }


}