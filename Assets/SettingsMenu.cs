using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;   

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public static SettingsMenu instance;

    public TMPro.TMP_Dropdown upDropdown;
    public TMPro.TMP_Dropdown downDropdown;
    public TMPro.TMP_Dropdown leftDropdown;
    public TMPro.TMP_Dropdown rightDropdown;

    public static string up;
    public static string down;
    public static string left;
    public static string right;

    private List<string> availableKeys = new List<string>();

    public string upKey1 = "UpKey";
    public string downKey1 = "DownKey";
    public string leftKey1 = "LeftKey";
    public string rightKey1 = "RightKey";


    public void SetFullScrean(bool isFullScrean)
    {
        Screen.fullScreen = isFullScrean;
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    void Start()
    {
        Debug.Log("UpKey: " + PlayerPrefs.GetString(upKey1));
        Debug.Log("DownKey: " + PlayerPrefs.GetString(downKey1));
        Debug.Log("LeftKey: " + PlayerPrefs.GetString(leftKey1));
        Debug.Log("RightKey: " + PlayerPrefs.GetString(rightKey1));

        foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode)))
        {
            if (keyCode != KeyCode.Mouse0 && keyCode != KeyCode.Mouse1 && keyCode != KeyCode.Mouse2 && keyCode != KeyCode.E)
            {
                availableKeys.Add(keyCode.ToString());
            }
        }

        upDropdown.ClearOptions();
        upDropdown.AddOptions(availableKeys);

        downDropdown.ClearOptions();
        downDropdown.AddOptions(availableKeys);

        leftDropdown.ClearOptions();
        leftDropdown.AddOptions(availableKeys);

        rightDropdown.ClearOptions();
        rightDropdown.AddOptions(availableKeys);

        if (!PlayerPrefs.HasKey(upKey1))
        {
            PlayerPrefs.SetString(upKey1, KeyCode.W.ToString());
        }

        if (!PlayerPrefs.HasKey(downKey1))
        {
            PlayerPrefs.SetString(downKey1, KeyCode.S.ToString());
        }

        if (!PlayerPrefs.HasKey(leftKey1))
        {
            PlayerPrefs.SetString(leftKey1, KeyCode.A.ToString());
        }

        if (!PlayerPrefs.HasKey(rightKey1))
        {
            PlayerPrefs.SetString(rightKey1, KeyCode.D.ToString());
        }

        upDropdown.value = availableKeys.IndexOf(PlayerPrefs.GetString(upKey1));
        downDropdown.value = availableKeys.IndexOf(PlayerPrefs.GetString(downKey1));
        leftDropdown.value = availableKeys.IndexOf(PlayerPrefs.GetString(leftKey1));
        rightDropdown.value = availableKeys.IndexOf(PlayerPrefs.GetString(rightKey1));
    }

    public void SaveMovementButtons()
    {
        KeySettings.instance.upKey = (KeyCode)System.Enum.Parse(typeof(KeyCode), upDropdown.options[upDropdown.value].text);
        KeySettings.instance.downKey = (KeyCode)System.Enum.Parse(typeof(KeyCode), downDropdown.options[downDropdown.value].text);
        KeySettings.instance.leftKey = (KeyCode)System.Enum.Parse(typeof(KeyCode), leftDropdown.options[leftDropdown.value].text);
        KeySettings.instance.rightKey = (KeyCode)System.Enum.Parse(typeof(KeyCode), rightDropdown.options[rightDropdown.value].text);

        PlayerPrefs.SetString("upKey", KeySettings.instance.upKey.ToString());
        PlayerPrefs.SetString("downKey", KeySettings.instance.downKey.ToString());
        PlayerPrefs.SetString("leftKey", KeySettings.instance.leftKey.ToString());
        PlayerPrefs.SetString("rightKey", KeySettings.instance.rightKey.ToString());
        PlayerPrefs.Save();

        up = upKey1;
        down = downKey1;
        left = leftKey1;
        right = rightKey1;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}


