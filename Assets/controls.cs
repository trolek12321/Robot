using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controls : MonoBehaviour
{
    public KeyCode defaultUpKey = KeyCode.W;
    public KeyCode defaultDownKey = KeyCode.S;
    public KeyCode defaultLeftKey = KeyCode.A;
    public KeyCode defaultRightKey = KeyCode.D;

    int i = 3;

    [Header("Movement")]
    public float movementSpeed;

    public float groundDrag;

    [Header("Ground")]
    public float playerHeight;
    public LayerMask Ground;
    bool grounded;

    public Transform orientation;

    float horizontalInput;
    float verticalInput;
    float fire1;

    Vector3 moveDirection;
    Rigidbody rg;

    private KeyCode upKey, downKey, leftKey, rightKey;

    private KeyCode upKey1, downKey1, leftKey1, rightKey1;

    private void Start()
    {
        rg = GetComponent<Rigidbody>();
        rg.freezeRotation = true;

        random_key.OnRandomKeysGenerated += HandleRandomKeysGenerated;
        LoadKeyBindings();
    }

    private void HandleRandomKeysGenerated(KeyCode up, KeyCode down, KeyCode left, KeyCode right)
    {
        upKey = up;
        downKey = down;
        leftKey = left;
        rightKey = right;
    }

    private void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, Ground);

        MyInput();
        SpeedControl();

        if (grounded)
        {
            rg.drag = groundDrag;
        }
        else
        {
            rg.drag = 0;
        }
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void MyInput()
    {
        if (random_key.walkingEnabled == false)
        {
            horizontalInput = Input.GetKey(leftKey) ? -1 : (Input.GetKey(rightKey) ? 1 : 0);
            verticalInput = Input.GetKey(downKey) ? -1 : (Input.GetKey(upKey) ? 1 : 0);
        }
        else
        {
            horizontalInput = Input.GetKey(leftKey1) ? -1 : (Input.GetKey(rightKey1) ? 1 : 0);
            verticalInput = Input.GetKey(downKey1) ? -1 : (Input.GetKey(upKey1) ? 1 : 0);
        }
    }

    private void MovePlayer()
    {
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        moveDirection.Normalize();

        rg.AddForce(moveDirection.normalized * movementSpeed * 5f, ForceMode.Force);

    }

    private void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rg.velocity.x, 0f, rg.velocity.y);
        flatVel.Normalize();

        if (flatVel.magnitude > movementSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * movementSpeed;
            rg.velocity = new Vector3(limitedVel.x, rg.velocity.y, limitedVel.z);
        }
    }

    private void LoadKeyBindings()
    {
        if (SettingsMenu.instance != null)
        {
            if (!string.IsNullOrEmpty(SettingsMenu.instance.upDropdown.options[SettingsMenu.instance.upDropdown.value].text))
            {
                random_key.walkingEnabled = true;
                upKey1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), SettingsMenu.instance.upDropdown.options[SettingsMenu.instance.upDropdown.value].text);
                downKey1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), SettingsMenu.instance.downDropdown.options[SettingsMenu.instance.downDropdown.value].text);
                leftKey1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), SettingsMenu.instance.leftDropdown.options[SettingsMenu.instance.leftDropdown.value].text);
                rightKey1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), SettingsMenu.instance.rightDropdown.options[SettingsMenu.instance.rightDropdown.value].text);
                return; // Zako�cz wczytywanie, je�li ustawienia zosta�y odnalezione
            }
        }

        // Ustaw domy�lne klawisze, je�li nie ma ustawionych warto�ci w menu ustawie� lub KeySettings
        upKey1 = defaultUpKey;
        downKey1 = defaultDownKey;
        leftKey1 = defaultLeftKey;
        rightKey1 = defaultRightKey;
    }
}
