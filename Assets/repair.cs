using System.Collections;
using UnityEngine;

public class repair : MonoBehaviour
{
    KeySettings keySettings = KeySettings.instance;
    bool playerInZone = false;
    bool isRepairing = false;
    public GameObject repairUI;
    public GameObject repairedUI;
    public random_key randomKeysScript;
    private System.Random random = new System.Random();
    private int randomNumber;
    public GameObject BrokeUI;

    /*private void OnTriggerEnter(Collider other)
    {
        playerInZone = true;
    }

    private void OnTriggerExit(Collider other)
    {
        playerInZone = false;
    }*/

    IEnumerator RepairProcess()
    {
        repairUI.SetActive(true);
        yield return new WaitForSeconds(3f);
        repairUI.SetActive(false);
        StartCoroutine(Broke());
        yield return new WaitForSeconds(2f);;
        if (randomNumber > 90)
        {
            BrokeUI.SetActive(true);
        }
        else
        {
            repairedUI.SetActive(true);
            random_key.walkingEnabled = true;
            random_key.timeStart = 10;
            random_key.randomNumber = 0;
            randomKeysScript.GetComponent<random_key>().StartCoroutine("RandomNumber12");
        }

        yield return new WaitForSeconds(2f);
        repairedUI.SetActive(false);
        BrokeUI.SetActive(false);
        yield return new WaitForSeconds(3f);
        Destroy(this);
    }

    private void Update()
    {
        /*if (playerInZone && Input.GetButton("Fire1") && !isRepairing)
        {
            StartCoroutine(RepairProcess());
            isRepairing = true;
        }*/
    }

    private void OnMouseDown()
    {
        if (!isRepairing && random_key.walkingEnabled == false) 
        {
            StartCoroutine(RepairProcess());
            isRepairing = true;
        }
    }

    IEnumerator Broke()
    {
        randomNumber = random.Next(1, 101);
        yield return new WaitForSeconds(2f);
    }
}