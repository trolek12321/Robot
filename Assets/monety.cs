using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monety : MonoBehaviour
{
    public GameObject moneta;
    public bool PlayerInZone;

    private void Start()
    {
        gameObject.GetComponent<Animator>().Play("switch");
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInZone = true;
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerInZone = false;
    }

    private void Update()
    {
        if (PlayerInZone)
        {
            MonetyManager.MonetyCount = MonetyManager.MonetyCount + 1;
            moneta.SetActive(false);
        }
    }
}
