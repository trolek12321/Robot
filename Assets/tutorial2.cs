using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class tutorial2 : MonoBehaviour
{
    public GameObject timeLeft;
    public GameObject timeNull;
    public GameObject kamera;
    public GameObject player;
    public GameObject tutorial1;
    public GameObject tutorial3;
    public GameObject wall;
    public GameObject box;
    public GameObject boxStop;

    public GameObject repairObject;
    private repair repairScript;

    public int look = 1;
    int timestart = 10;

    public TMPro.TextMeshProUGUI textMeshPro;

    bool wallDisable = false;

    private void Start()
    {
        repairScript = repairObject.GetComponent<repair>();
        if (repairScript != null)
        {
            Debug.Log("Found repair script."); // Znaleziono skrypt repair
        }
        else
        {
            Debug.Log("Repair script not found."); // Nie znaleziono skryptu repair
        }
        timeNull.SetActive(false);
        boxStop.SetActive(false);
        StartCoroutine("TimeStart");
    }

    private void Update()
    {
        if (wallDisable == true)
        {
            if (repairScript != null && repairScript.enabled)
            {
                boxStop.SetActive(true);
            }
            else
            {
                boxStop.SetActive(false);
                timeNull.SetActive(true);
                StartCoroutine("NextTutorial");
                wallDisable = false;
            }
        }
    }

    IEnumerator NextTutorial()
    {
        while (look > 0)
        {
            look--;
            if (look == 0)
            {
                timeNull.SetActive(false);
                tutorial1.SetActive(false);
                tutorial3.SetActive(true);
                kamera.transform.position = new Vector3(76, 2, -20);
                player.transform.position = new Vector3(83, -1, -19);
                yield break;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator TimeStart()
    {
        while (timestart > 0)
        {
            timestart--;
            textMeshPro.text = timestart.ToString();
            if (timestart == 0)
            {
                timeLeft.SetActive(false);
                wallDisable = true;
                wall.SetActive(false);
                yield break;
            }
            yield return new WaitForSeconds(1f);
        }
    }
}
