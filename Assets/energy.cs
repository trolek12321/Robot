using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class energy : MonoBehaviour
{
    public static float main_energy = 300;
    public Slider EnergySlider;

    void Start()
    {
        EnergySlider = GameObject.Find("EnergySlider").GetComponent<Slider>();
        EnergySlider.maxValue = main_energy;

        // Rozpocznij coroutine, kt�ry kontroluje spadek energii
        StartCoroutine(Energy());
    }

    IEnumerator Energy()
    {
        while (main_energy > 0)
        {
            // Zmniejsz energi�
            main_energy -= 3;
            // Aktualizuj warto�� Slidera
            EnergySlider.value = main_energy;
            // Poczekaj 1 sekund�
            yield return new WaitForSeconds(1f);
        }

        // Po zako�czeniu energii wczytaj kolejn� scen�
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
