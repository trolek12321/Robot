using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class random_key : MonoBehaviour
{
    KeySettings keySettings = KeySettings.instance;
    public static event Action<KeyCode, KeyCode, KeyCode, KeyCode> OnRandomKeysGenerated;
    private System.Random random = new System.Random();
    public TMPro.TextMeshProUGUI textTmp1;
    public static bool walkingEnabled = true;
    public static int randomNumber;
    public static int timeStart;
    public int time;

    private List<KeyCode> possibleKeys = new List<KeyCode>
    {
        KeyCode.A, KeyCode.B, KeyCode.C, KeyCode.D, KeyCode.F,
        KeyCode.G, KeyCode.H, KeyCode.I, KeyCode.J, KeyCode.K,
        KeyCode.L, KeyCode.M, KeyCode.N, KeyCode.O, KeyCode.P,
        KeyCode.Q, KeyCode.R, KeyCode.S, KeyCode.T, KeyCode.U,
        KeyCode.V, KeyCode.W, KeyCode.X, KeyCode.Y, KeyCode.Z,
        KeyCode.Alpha0, KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3,
        KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7,
        KeyCode.Alpha8, KeyCode.Alpha9
    };

    void Awake()
    {
        timeStart = time;
        StartCoroutine(RandomNumber12());
    }

    public IEnumerator RandomNumber12()
    {
        while(timeStart>0)
        {
            time--;
            yield return new WaitForSeconds(1f);

            textTmp1.text = timeStart.ToString();
            if (timeStart == 0)
            {
                walkingEnabled = false;
                ChangeWalkingKeys();
            }
        }
    }

    void Update()
    {
        timeStart = time;
    }

    void ChangeWalkingKeys()
    {
        List<KeyCode> uniqueKeys = new List<KeyCode>();
        while (uniqueKeys.Count < 4)
        {
            KeyCode randomKey = possibleKeys[UnityEngine.Random.Range(0, possibleKeys.Count)];
            if (!uniqueKeys.Contains(randomKey))
            {
                uniqueKeys.Add(randomKey);
            }
        }

        KeyCode upKey = uniqueKeys[0];
        KeyCode downKey = uniqueKeys[1];
        KeyCode leftKey = uniqueKeys[2];
        KeyCode rightKey = uniqueKeys[3];

        Debug.Log("Zmieniono klawisze do chodzenia: " + upKey + " (do g�ry), " + downKey + " (w d�), " + leftKey + " (w lewo), " + rightKey + " (w prawo)");

        OnRandomKeysGenerated?.Invoke(upKey, downKey, leftKey, rightKey);
    }
}
