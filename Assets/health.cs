using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class health : MonoBehaviour
{
    public static float main_health = 100;
    private bool healthSubtracted = false;
    public Slider healthSlider;
    public random_key random_key;
    private void Start()
    {
        healthSlider = GameObject.Find("HealthSlider").GetComponent<Slider>();
        healthSlider.maxValue = main_health;
    }

    private void Update()
    {
        if (random_key.walkingEnabled == false && !healthSubtracted)
        {
            main_health -= 20f;
            healthSubtracted = true;
        }

        if(random_key.walkingEnabled == true)
        {
            healthSubtracted = false;
        }

        healthSlider.value = main_health;

        if (main_health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

}
