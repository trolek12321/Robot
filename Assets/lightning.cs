using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightning : MonoBehaviour
{
    public bool PlayerInZone;
    public GameObject lightnings;
    public bool EnergyAdded = false;
    private void Start()
    {
        gameObject.GetComponent<Animator>().Play("switch");
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInZone = true;
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerInZone = false;
        EnergyAdded = false;
    }

    private void Update()
    {
        if(PlayerInZone && !EnergyAdded && energy.main_energy < 300)
        {
            float energyToAdd = Mathf.Min(300 - energy.main_energy, 30);
            energy.main_energy += energyToAdd;
            EnergyAdded = true;
            lightnings.SetActive(false);
        }
    }
}
