using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class tutorial5 : MonoBehaviour
{
    bool MonetyNull = false;
    int i = 5;

    public GameObject wall;
    public GameObject tutorialEnd;
    public GameObject FiveCoins;
    public GameObject shop;
    void Start()
    {
        tutorialEnd.SetActive(false);
        shop.SetActive(false);
        health.main_health -= 20f;
    }

    void Update()
    {
        if(MonetyManager.MonetyCount == 5 && MonetyNull == false)
        {
            wall.SetActive(false);
            FiveCoins.SetActive(false);
            shop.SetActive(true);
            MonetyManager.MonetyCount = 15;
            MonetyNull = true;
        }
        if(MonetyManager.MonetyCount < 15 && MonetyNull == true)
        {
            StartCoroutine("TutorialEnd");
        }
    }

    IEnumerator TutorialEnd()
    {
        while(i > 0) 
        {
            i--;
            tutorialEnd.SetActive(true);
            shop.SetActive(false);
            if(i == 0)
            {
                SceneManager.LoadScene("Start");
            }
            yield return null;
        }
    }
}
