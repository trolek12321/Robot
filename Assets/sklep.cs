using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sklep : MonoBehaviour
{
    public GameObject sklepUI;
    public GameObject cursor;
    public GameObject NotEnoughMoney;
    public TMPro.TextMeshProUGUI textMeshPro;
    void Start()
    {
        sklepUI.SetActive(false);
    }

    // Update is called once per frame
    private void OnMouseDown()
    {
        sklepUI.SetActive(true);
        Time.timeScale = 0f;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        cursor.SetActive(false);
    }

    public void Resume()
    {
        sklepUI.SetActive(false);
        Time.timeScale = 1f;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        cursor.SetActive(true);
    }
    
    public void CzasButton()
    {
        if(MonetyManager.MonetyCount >= 5)
        {
            MonetyManager.MonetyCount = MonetyManager.MonetyCount - 5;
            random_key.timeStart = random_key.timeStart + 5;
            textMeshPro.text = random_key.timeStart.ToString();
        }
        else
        {
            StartCoroutine("NotEnough");
        }
    }

    public void EnergyButton()
    {
        if(MonetyManager.MonetyCount >= 10)
        {
            MonetyManager.MonetyCount = MonetyManager.MonetyCount - 10;
            energy.main_energy = energy.main_energy + 30;
        }
        else
        {
            StartCoroutine("NotEnough");
        }
    }

    public void HPButton()
    {
        if (MonetyManager.MonetyCount >= 15)
        {
            MonetyManager.MonetyCount = MonetyManager.MonetyCount - 15;
            health.main_health = health.main_health + 40;
        }
        else
        {
            StartCoroutine("NotEnough");
        }
    }

    IEnumerator NotEnough()
    {
        NotEnoughMoney.SetActive(true);
        yield return new WaitForSeconds(2f);
        NotEnoughMoney.SetActive(false);
        yield break;
    }
}
