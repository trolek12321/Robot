using UnityEngine;
using System.Collections.Generic;

public class MazeGenerator : MonoBehaviour
{
    public int mazeWidth = 10;
    public int mazeHeight = 10;
    public float cellSize = 1.0f;
    public GameObject wallPrefab;
    public GameObject floorPrefab;
    public GameObject ceilingPrefab;
    public GameObject victoryPointPrefab;
    public GameObject[] playerPrefabs;
    public GameObject[] boxPrefabs;
    public GameObject shopPrefab;
    public GameObject[] cluePrefabs;
    public GameObject[] findablePrefabs;
    public int minBoxes = 5;
    public int maxBoxes = 10;
    public int minClues = 5;
    public int maxClues = 8;
    public int minFindables = 15;
    public int maxFindables = 45;
    public Transform cameraTransform;
    public int maxWalls = 50; // Maksymalna liczba �cian do wygenerowania
    public int maxEmptySpaces = 50; // Maksymalna liczba pustych przestrzeni

    private int[,] maze;
    private int currentWallCount = 0; // Licznik wygenerowanych �cian
    private int currentEmptySpaceCount = 0; // Licznik pustych przestrzeni

    void Start()
    {
        GenerateMaze();
        DrawMaze();
        DrawOuterWalls();
        SpawnVictoryPoint();
        SpawnPlayer();
        SpawnBoxes();
        SpawnShop();
        SpawnClues();
        SpawnFindables();
    }

    void GenerateMaze()
    {
        maze = new int[mazeWidth, mazeHeight];
        for (int x = 0; x < mazeWidth; x++)
        {
            for (int y = 0; y < mazeHeight; y++)
            {
                maze[x, y] = 1; // Wype�niamy ca�� map� �cianami
            }
        }

        Vector2Int startPoint = new Vector2Int(1, 1);
        Vector2Int endPoint = new Vector2Int(mazeWidth - 2, mazeHeight - 2);
        RecursiveBacktracker(startPoint, endPoint);

        // Losowo rozmieszczamy �ciany na mapie
        int wallCount = 0;
        while (wallCount < maxWalls)
        {
            Vector2Int randomPosition = FindRandomEmptyCellWithinBounds();
            if (randomPosition != startPoint && randomPosition != endPoint)
            {
                maze[randomPosition.x, randomPosition.y] = 1; // Ustawiamy �cian�
                wallCount++;
            }
        }
    }

    void RecursiveBacktracker(Vector2Int currentPosition, Vector2Int endPoint)
    {
        maze[currentPosition.x, currentPosition.y] = 0; // Ustawiamy startow� pozycj� jako drog�

        int steps = 0;
        while (currentPosition != endPoint)
        {
            List<Vector2Int> availableDirections = new List<Vector2Int>();

            // Sprawdzamy mo�liwe kierunki ruchu
            if (currentPosition.x < mazeWidth - 2 && maze[currentPosition.x + 2, currentPosition.y] == 1)
            {
                availableDirections.Add(Vector2Int.right);
            }
            if (currentPosition.y < mazeHeight - 2 && maze[currentPosition.x, currentPosition.y + 2] == 1)
            {
                availableDirections.Add(Vector2Int.up);
            }
            if (currentPosition.x > 1 && maze[currentPosition.x - 2, currentPosition.y] == 1)
            {
                availableDirections.Add(Vector2Int.left);
            }
            if (currentPosition.y > 1 && maze[currentPosition.x, currentPosition.y - 2] == 1)
            {
                availableDirections.Add(Vector2Int.down);
            }

            if (availableDirections.Count == 0)
            {
                break; // Nie ma dost�pnych kierunk�w
            }

            // Losowo wybieramy jeden z dost�pnych kierunk�w
            Vector2Int selectedDirection = availableDirections[Random.Range(0, availableDirections.Count)];

            // Losowo zmieniamy kierunek co kilka krok�w
            if (Random.value < 0.2f && steps > 2)
            {
                selectedDirection = availableDirections[Random.Range(0, availableDirections.Count)];
                steps = 0; // Resetujemy licznik krok�w
            }

            // Losowo wybieramy d�ugo�� kroku
            int stepLength = Random.Range(1, 3);

            // Przesuwamy si� o wybran� d�ugo�� w wybranym kierunku
            currentPosition += selectedDirection * stepLength;

            // Oznaczamy aktualn� pozycj� jako drog�
            maze[currentPosition.x, currentPosition.y] = 0;

            steps++;
        }
    }

    void DrawMaze()
    {
        List<Vector2Int> wallPositions = new List<Vector2Int>();

        for (int x = 0; x < mazeWidth; x++)
        {
            for (int y = 0; y < mazeHeight; y++)
            {
                if (maze[x, y] == 1)
                {
                    wallPositions.Add(new Vector2Int(x, y));
                }
            }
        }

        ShuffleList(wallPositions);

        foreach (Vector2Int pos in wallPositions)
        {
            if (currentWallCount >= maxWalls)
            {
                return;
            }

            DrawWalls(pos.x, pos.y, new Vector3(pos.x * cellSize, 0, pos.y * cellSize));
        }
    }

    void DrawWalls(int x, int y, Vector3 position)
    {
        if (currentWallCount >= maxWalls)
        {
            return; // Przerwij, je�li osi�gni�to maksymaln� liczb� �cian
        }

        int wallLength = Random.Range(1, 4); // Random length between 1 and 3
        if (x == 0 || IsWallSegment(x - 1, y, wallLength, Vector2Int.left))
        {
            for (int i = 0; i < wallLength; i++)
            {
                Instantiate(wallPrefab, position + new Vector3(-cellSize * i / 2, 0, 0), Quaternion.identity); // Left wall
                currentWallCount++;
                if (currentWallCount >= maxWalls)
                {
                    return; // Przerwij, je�li osi�gni�to maksymaln� liczb� �cian
                }
            }
        }
        if (x == mazeWidth - 1 || IsWallSegment(x + 1, y, wallLength, Vector2Int.right))
        {
            for (int i = 0; i < wallLength; i++)
            {
                Instantiate(wallPrefab, position + new Vector3(cellSize * i / 2, 0, 0), Quaternion.identity); // Right wall
                currentWallCount++;
                if (currentWallCount >= maxWalls)
                {
                    return; // Przerwij, je�li osi�gni�to maksymaln� liczb� �cian
                }
            }
        }
        if (y == 0 || IsWallSegment(x, y - 1, wallLength, Vector2Int.down))
        {
            for (int i = 0; i < wallLength; i++)
            {
                Instantiate(wallPrefab, position + new Vector3(0, 0, -cellSize * i / 2), Quaternion.identity); // Bottom wall
                currentWallCount++;
                if (currentWallCount >= maxWalls)
                {
                    return; // Przerwij, je�li osi�gni�to maksymaln� liczb� �cian
                }
            }
        }
        if (y == mazeHeight - 1 || IsWallSegment(x, y + 1, wallLength, Vector2Int.up))
        {
            for (int i = 0; i < wallLength; i++)
            {
                Instantiate(wallPrefab, position + new Vector3(0, 0, cellSize * i / 2), Quaternion.identity); // Top wall
                currentWallCount++;
                if (currentWallCount >= maxWalls)
                {
                    return; // Przerwij, je�li osi�gni�to maksymaln� liczb� �cian
                }
            }
        }
    }

    bool IsWallSegment(int x, int y, int length, Vector2Int direction)
    {
        for (int i = 0; i < length; i++)
        {
            int nx = x + direction.x * i;
            int ny = y + direction.y * i;
            if (!IsInBounds(new Vector2Int(nx, ny)) || maze[nx, ny] == 1)
            {
                return true;
            }
        }
        return false;
    }

    void DrawOuterWalls()
    {
        for (int x = 0; x < mazeWidth; x++)
        {
            Vector3 positionBottom = new Vector3(x * cellSize, 0, 0);
            Vector3 positionTop = new Vector3(x * cellSize, 0, (mazeHeight - 1) * cellSize);
            Instantiate(wallPrefab, positionBottom, Quaternion.identity);
            Instantiate(wallPrefab, positionTop, Quaternion.identity);
        }

        for (int y = 0; y < mazeHeight; y++)
        {
            Vector3 positionLeft = new Vector3(0, 0, y * cellSize);
            Vector3 positionRight = new Vector3((mazeWidth - 1) * cellSize, 0, y * cellSize);
            Instantiate(wallPrefab, positionLeft, Quaternion.identity);
            Instantiate(wallPrefab, positionRight, Quaternion.identity);
        }
    }

    void SpawnVictoryPoint()
    {
        Vector2Int victoryPointPosition = new Vector2Int(mazeWidth - 2, mazeHeight - 2);
        Vector3 position = new Vector3(victoryPointPosition.x * cellSize, -3, victoryPointPosition.y * cellSize); // Zmiana wysoko�ci na -3
        Debug.Log("Victory point position: " + position);
        Instantiate(victoryPointPrefab, position, Quaternion.identity);
    }

    void SpawnPlayer()
    {
        Vector2Int startPosition = new Vector2Int(1, 1);
        Vector3 position = new Vector3(startPosition.x * cellSize, 0, startPosition.y * cellSize);
        GameObject playerPrefab = playerPrefabs[Random.Range(0, playerPrefabs.Length)];
        GameObject player = Instantiate(playerPrefab, position, Quaternion.identity);
        player.SetActive(true);
        if (cameraTransform != null)
        {
            cameraTransform.position = position + new Vector3(0, 0, 0);
            cameraTransform.rotation = Quaternion.identity;
        }
    }

    void SpawnBoxes()
    {
        int numBoxes = Random.Range(minBoxes, maxBoxes + 1);
        Debug.Log("Spawning " + numBoxes + " boxes.");
        for (int i = 0; i < numBoxes; i++)
        {
            Vector2Int boxPosition = FindRandomEmptyCellWithinBounds();
            Vector3 position = new Vector3(boxPosition.x * cellSize, -2.53f, boxPosition.y * cellSize); // Zmiana wysoko�ci na -2.53
            Debug.Log("Box " + i + " position: " + position);
            GameObject boxPrefab = boxPrefabs[Random.Range(0, boxPrefabs.Length)];
            Instantiate(boxPrefab, position, Quaternion.identity);
        }
    }

    void SpawnShop()
    {
        Vector2Int shopPosition = FindRandomEmptyCellWithinBounds();
        Vector3 position = new Vector3(shopPosition.x * cellSize, -2.5f, shopPosition.y * cellSize); // Spawn at y = -2
        Instantiate(shopPrefab, position, Quaternion.identity);
    }

    void SpawnClues()
    {
        int numClues = Random.Range(minClues, maxClues + 1);
        for (int i = 0; i < numClues; i++)
        {
            Vector2Int cluePosition = FindRandomEmptyCellWithinBounds();
            Vector3 position = new Vector3(cluePosition.x * cellSize, -2, cluePosition.y * cellSize); // Spawn at y = -2
            GameObject cluePrefab = cluePrefabs[Random.Range(0, cluePrefabs.Length)];
            Instantiate(cluePrefab, position, Quaternion.identity);
        }
    }

    void SpawnFindables()
    {
        int numFindables = Random.Range(minFindables, maxFindables + 1);
        for (int i = 0; i < numFindables; i++)
        {
            Vector2Int findablePosition = FindRandomEmptyCellWithinBounds();
            Vector3 position = new Vector3(findablePosition.x * cellSize, -2, findablePosition.y * cellSize); // Spawn at y = -2
            GameObject findablePrefab = findablePrefabs[Random.Range(0, findablePrefabs.Length)];
            Instantiate(findablePrefab, position, Quaternion.identity);
        }
    }

    Vector2Int FindRandomEmptyCellWithinBounds()
    {
        Vector2Int randomCell;
        do
        {
            randomCell = new Vector2Int(Random.Range(1, mazeWidth - 1), Random.Range(1, mazeHeight - 1));
        } while (maze[randomCell.x, randomCell.y] != 0);

        return randomCell;
    }

    bool IsInBounds(Vector2Int cell)
    {
        return cell.x >= 0 && cell.x < mazeWidth && cell.y >= 0 && cell.y < mazeHeight;
    }

    void ShuffleList<T>(List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            int randomIndex = Random.Range(i, list.Count);
            T temp = list[i];
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }
}
