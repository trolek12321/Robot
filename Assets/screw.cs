using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class screw : MonoBehaviour
{
    public health health;
    public bool PlayerInZone;
    public bool HealthAdded = false;
    public GameObject screws;

    private void Start()
    {
        gameObject.GetComponent<Animator>().Play("switch");
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInZone = true;
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerInZone = false;
        HealthAdded = false;
    }

    private void Update()
    {
        if(PlayerInZone && HealthAdded == false && health.main_health <= 80) 
        {
            health.main_health = health.main_health + 20;
            HealthAdded = true;
            screws.SetActive(false);
        }
    }
}
